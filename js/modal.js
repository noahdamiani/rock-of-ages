$(document).ready(function() {
	    $('#pop, #pop-two').bind('click', function(e) { 
	        e.preventDefault();
	        $('#modal-window').bPopup();
	    });
	    $('.closer').bind('click', function(e) {
	    	$('#modal-window').bPopup().close();
	    });
	     $('#pop-thanks').bind('click', function(e) { 
	        e.preventDefault();
	        $('#thank-you').bPopup();
	        $('#modal-window').bPopup().close();
	    });
	    $('.closer').bind('click', function(e) {
	    	$('#thank-you').bPopup().close();
	    });
	});