-- Adminer 3.7.1 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = '-05:00';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `rockofages` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rockofages`;

DROP TABLE IF EXISTS `signups`;
CREATE TABLE `signups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `signups` (`id`, `first_name`, `last_name`, `mail`, `created_at`) VALUES
(1,	'KG',	'Test',	'kgeane@rocknroll.com',	'2013-08-28 13:36:57'),
(2,	'Danielle',	'Migliaccio',	'dmigliaccio@situationinteractive.com',	'2013-08-28 15:59:05'),
(3,	'Miriam',	'Naggar',	'mnaggar@gmail.com',	'2013-08-28 17:27:20'),
(4,	'miriam',	'naggar',	'miriamn11@hotmail.com',	'2013-08-28 17:27:40'),
(5,	'miraim',	'n',	'mn@gmail.com',	'2013-08-28 17:36:13'),
(6,	'Nicole',	'Lemieux',	'nlemieux@situationinteractive.com',	'2013-08-28 17:50:22'),
(7,	'fgfg',	'fg',	'fggffg@dfdf.com',	'2013-08-29 10:33:44'),
(8,	'ghhg',	'grg',	'grt@g.com',	'2013-08-29 10:53:51'),
(9,	'test',	'test',	'',	'2013-08-29 10:55:19'),
(10,	'test',	'test',	'',	'2013-08-29 10:56:58'),
(11,	'noah',	'test',	'',	'2013-08-30 09:19:09');

-- 2013-08-30 10:16:01
